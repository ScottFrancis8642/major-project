﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Customization : Photon.MonoBehaviour 
{
	public static Customization instance;

	//public GameObject glove;
	public Material[] colorRedMaterials;
	public Material[] colorBlueMaterials;
	public Material[] colorBlackMaterials;
	public Material[] colorYellowMaterials;

	public Image[] playerHUDBGIconPlaceHolders;

	public int shipColour;

	//public int currentColorID;

	public int[] playerColors;


	void Awake ()
	{
		instance = this;
	}

	void Start()
	{
		shipColour = 0;
	}

	public void ChooseLeftColour()
	{
		//currentColorID--;
		shipColour--;

		if (shipColour < 0) {
			shipColour = 3;
		}

		PhotonNetwork.RPC (gameObject.GetPhotonView (), "UpdatePlayerColorNums", PhotonTargets.AllBuffered, false, PhotonNetwork.player.ID-1, shipColour);
	}
		
	public void ChooseRightColour()
	{
		
		shipColour++;

		if (shipColour > 3) 
		{
			shipColour = 0;
		}
		PhotonNetwork.RPC (gameObject.GetPhotonView (), "UpdatePlayerColorNums", PhotonTargets.AllBuffered, false, PhotonNetwork.player.ID-1, shipColour);
	}

	[PunRPC]
	public void UpdatePlayerColorNums(int playerNum, int playerCol)
	{
		playerColors [playerNum] = playerCol;

	}

//    
		
}
