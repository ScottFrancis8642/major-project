﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TransitionToMenus : MonoBehaviour 
{
	public void ButtonEvent()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif
	}
}
