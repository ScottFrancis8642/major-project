using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon;
/// <summary>
/// Can be attached to a GameObject to show info about the owner of the PhotonView.
/// </summary>
/// <remarks>
/// This is a Photon.Monobehaviour, which adds the property photonView (that's all).
/// </remarks>
[RequireComponent(typeof(PhotonView))]
public class ShowInfoOfPlayer : Photon.MonoBehaviour
{
    private GameObject textGo;
    private TextMesh tm;
    public float CharacterSize = 0;
	public Camera cameraToLookAt;

    public Font font;
    public bool DisableOnOwnObjects;
	//public Camera cameraToLookAt;

    void Start()
    {
		

        if (font == null)
        {
            #if UNITY_3_5
            font = (Font)FindObjectsOfTypeIncludingAssets(typeof(Font))[0];
            #else
            font = (Font)Resources.FindObjectsOfTypeAll(typeof(Font))[0];
            #endif
            Debug.LogWarning("No font defined. Found font: " + font);
        }

        if (tm == null)
        {
            textGo = new GameObject("3d text");
            //textGo.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            textGo.transform.parent = this.gameObject.transform;
			textGo.transform.localPosition = new Vector3 (0, 0.6f, 0);

            MeshRenderer mr = textGo.AddComponent<MeshRenderer>();
            mr.material = font.material;
            tm = textGo.AddComponent<TextMesh>();
            tm.font = font;
            tm.anchor = TextAnchor.MiddleCenter;
            if (this.CharacterSize > 0)
            {
                tm.characterSize = this.CharacterSize;
            }
        }
    }

	void BillboardTowardsUs()
	{
		if (cameraToLookAt != null) {
			Vector3 v = cameraToLookAt.transform.position - textGo.transform.position;
			v.x = v.z = 0.0f;
			textGo.transform.LookAt (cameraToLookAt.transform.position - v); 
			textGo.transform.Rotate (0, 180, 0);
		} 
		else 
		{
			Camera[] cams = GameObject.FindObjectsOfType<Camera> ();
				foreach (Camera c in cams) 
				{
					if (c.gameObject.activeSelf && c.gameObject.transform.root != this.transform.root)
						cameraToLookAt = c;
				}
		}
		if (cameraToLookAt != null && cameraToLookAt.transform.root == this.transform.root)
			cameraToLookAt = null;
	}

    void Update()
    {
        bool showInfo = !this.DisableOnOwnObjects || this.photonView.isMine;
        if (textGo != null)
        {
            textGo.SetActive(showInfo);
			BillboardTowardsUs ();
        }
        if (!showInfo)
        {
            return;
        }

        
        PhotonPlayer owner = this.photonView.owner;
        if (owner != null)
        {
            tm.text = (string.IsNullOrEmpty(owner.name)) ? "player"+owner.ID : owner.name;
        }
        else if (this.photonView.isSceneView)
        {
            tm.text = "scn";
        }
        else
        {
            tm.text = "n/a";
        }



    }


}
