﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationManager : Photon.MonoBehaviour {

	public Animator myAnimator;
	public Animator proxyAnimator;

	public float avgX;
	public List<float> avgXvalues = new List<float>();
	public float avgY;
	public List<float> avgYvalues = new List<float>();

	public float sampleRate = 1;
	float sampleRateTime;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		sampleRateTime -= Time.deltaTime;
		avgXvalues.Add (GetComponentInChildren<CamMouseLook> ().speedX);
		avgYvalues.Add (GetComponentInChildren<CamMouseLook> ().speedY);

		if (sampleRateTime <= 0) 
		{
			sampleRateTime = sampleRate;
			//Caluclate average
			avgX= CalculateAverageX();
			avgXvalues.Clear ();
			avgY= CalculateAverageY();
			avgYvalues.Clear ();

		}

		if (photonView.isMine) 
		{
			myAnimator.SetFloat ("X", Mathf.Clamp(GetComponentInChildren<CamMouseLook>().speedX,-100,100),1f,Time.deltaTime*10);


			myAnimator.SetFloat ("Y", Mathf.Clamp(GetComponentInChildren<CamMouseLook>().speedY,-100,100),1f,Time.deltaTime*10);

		} 

	}

	float CalculateAverageX()
	{
		float avgx = 0;

		for (int i = 0; i < avgXvalues.Count; i++) 
		{
			avgX += avgXvalues [i];
		}

		if (avgX < 0.05f && avgX > -0.05f) 
		{
			return 0;
		}
		return avgX / avgXvalues.Count;
	}

	float CalculateAverageY()
	{
		float avgy = 0;

		for (int i = 0; i < avgYvalues.Count; i++) 
		{
			avgY += avgYvalues [i];
		}

		if (avgY < 0.05f && avgY > -0.05f) 
		{
			return 0;
		}
		return avgY / avgYvalues.Count;
	}
		

	[PunRPC]
	public void PlayPunchAnimation ()
	{
		if (photonView.isMine) 
		{
			myAnimator.SetTrigger ("Punch");
		} 
		else 
		{
			proxyAnimator.SetTrigger ("Punch");
		}
	}



}
