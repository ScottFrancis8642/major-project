﻿using UnityEngine;
using System.Collections;

public class BoostPad : MonoBehaviour {

	public float velocity = 10.0f;
	public float force = 25.0f;
	private Rigidbody rBody;
	public GameObject player;

	// Use this for initialization
	void Start () {
		
		rBody = player.GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider c)
	{
		if (rBody.tag == "Player")
		{
			rBody.velocity = Vector3.up * velocity;
		}
	}

	void OnTriggerExit(Collider c)
	{
		if (rBody.tag == "Player")
		{
			rBody.AddForce (0, force, 0);
		}
	}
}
