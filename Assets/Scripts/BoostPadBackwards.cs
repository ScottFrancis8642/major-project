﻿using UnityEngine;
using System.Collections;

public class BoostPadBackwards : MonoBehaviour {

	public float velocity = 100.0f;
	public float force = 1000.0f;
	private Rigidbody rBody;
	public GameObject player;

	// Use this for initialization
	void Start () {

		rBody = player.GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider c)
	{
		if (rBody.tag == "Player")
		{
			rBody.velocity = Vector3.up * velocity;
		}
	}

	void OnTriggerExit(Collider c)
	{
		if (rBody.tag == "Player")
		{
			rBody.AddForce (-800, force, 0);
		}
	}
}
