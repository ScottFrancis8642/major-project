﻿using UnityEngine;
using System.Collections;

public class BoostPadForward : MonoBehaviour {

	public float velocity = 10.0f;
	public float force = 30.0f;

	private GameObject player;

	// Use this for initialization

	void Awake ()
	{
		
	}

	void Start () 
	{

		player = GameObject.FindGameObjectWithTag ("Player");


	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.up * velocity;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			other.gameObject.GetComponent<Rigidbody>().AddRelativeForce (0, force, 0);
		}
	}
}
