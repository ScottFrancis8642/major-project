﻿using UnityEngine;
using System.Collections;

public class CamMouseLook : Photon.MonoBehaviour {

	Vector2 mouseLook;
	Vector2 smoothV;
	public float sensitivity = 5.0f;
	public float smoothing = 2.0f;
	public float speedX;
	public float speedY;



	private GameObject character;

	void Start () 
	{
		character = this.transform.parent.gameObject;

	}
	

	void Update () 
	{
		

		speedX = Input.GetAxis ("Mouse X") / Time.deltaTime;
		speedY = Input.GetAxis ("Mouse Y") / Time.deltaTime;

		var md = new Vector2 (Input.GetAxisRaw ("Mouse X"), Input.GetAxisRaw ("Mouse Y"));

			md = Vector2.Scale(md, new Vector2 (sensitivity * smoothing, sensitivity * smoothing));
		smoothV.x = Mathf.Lerp (smoothV.x, md.x, 1f / smoothing);
		smoothV.y = Mathf.Lerp (smoothV.y, md.y, 1f / smoothing);
		mouseLook += smoothV;


		transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
		character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, Vector3.up);


	}


}
