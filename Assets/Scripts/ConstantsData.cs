﻿using UnityEngine;
using System.Collections;

public class ConstantsData : MonoBehaviour 
{
	/// <summary>
	/// The Last hit timer.
	/// </summary>
	public const float LAST_HIT_TIMER = 8;
}
