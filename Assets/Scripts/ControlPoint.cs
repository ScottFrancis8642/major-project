﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ControlPoint : Photon.MonoBehaviour 
{

	public GameObject me;
    public GameObject crownCopy;


	public GameObject[] playersInGame;
	public List<NetworkPlayer> playersInRange = new List<NetworkPlayer> ();

	public ParticleSystem controlPointPS;



	void Update () 
	{
		playersInGame = GameObject.FindGameObjectsWithTag("Player");


		foreach (GameObject p in playersInGame) 
		{
			//Inside
			if (Vector3.Distance (this.transform.position, p.transform.position) < 5) 
			{
				if (!playersInRange.Contains (p.GetComponent<NetworkPlayer> ())) 
				{
					playersInRange.Add (p.GetComponent<NetworkPlayer> ());
				}
			}
			else
			{
				if (playersInRange.Contains (p.GetComponent<NetworkPlayer> ())) 
				{
					playersInRange.Remove (p.GetComponent<NetworkPlayer> ());
				}
			}



		}

		playersInRange = playersInRange.Where (item => item != null).ToList ();

		if(me != null)
		{
            //Increase that players %
			if (playersInRange.Count.Equals(1) && playersInRange.Contains(me.GetComponent<NetworkPlayer>()))
            {
                //playersInRange[0].GetComponent<PhotonView>().RPC("IncreaseControlPoints", PhotonTargets.All);
				playersInRange [0].GetComponent<NetworkPlayer> ().IncreaseControlPoints ();
				UIManager.instance.controlPointsRadial.fillAmount = playersInRange[0].controlPoints / 100;
            }
		}
	}
}
