﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

	public Texture2D crosshairImage;
	public Texture2D fistImage;
	public bool showGUI = false;

	void OnGUI ()
	{
		float xMinCrosshair = (Screen.width / 2) - (crosshairImage.width / 2);
		float yMinCrosshair = (Screen.height / 2) - (crosshairImage.height / 2);
		GUI.DrawTexture (new Rect (xMinCrosshair, yMinCrosshair, crosshairImage.width, crosshairImage.height), crosshairImage);

		if (showGUI == true) 
		{
			StartCoroutine ("HitMarker");
			float xMinFist = (Screen.width / 2) - (fistImage.width / 2);
			float yMinFist = (Screen.height / 2) - (fistImage.height / 2);

			GUI.DrawTexture (new Rect ((Screen.width / 2) - 95, (Screen.height / 2) - 75, fistImage.width / 6, fistImage.height / 6), fistImage, ScaleMode.ScaleToFit);
		}
	}

	IEnumerator HitMarker ()
	{
		yield return new WaitForSeconds (0.2f);
		showGUI = false;
	}

}
