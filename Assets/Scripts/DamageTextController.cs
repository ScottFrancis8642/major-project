﻿using UnityEngine;
using System.Collections;

public class DamageTextController : Photon.MonoBehaviour {

	private static DamageTextPopUp popUpText;
	private static Canvas canvas;
	public static Camera playercam;

	public static void Initialise ()
	{
		canvas = GameObject.Find ("DefaultPlayer").GetComponentInChildren<Canvas>();

		//if (!popUpText)
		popUpText = Resources.Load<DamageTextPopUp> ("DamageTextParent");
	}

	public static void CreateFloatingText (string text, Transform location)
	{
		DamageTextPopUp instance = Instantiate (popUpText) as DamageTextPopUp;
	//	Debug.Log (Camera.main.transform.position + "Camerapos");
		//Vector2 screenPosition = playercam.WorldToScreenPoint (location.position);

		instance.transform.SetParent (canvas.transform, false);
		//instance.transform.position = screenPosition;
		instance.transform.position = location.position;
		instance.SetText (text);
	}
}
