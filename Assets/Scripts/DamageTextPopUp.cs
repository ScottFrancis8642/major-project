﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DamageTextPopUp : MonoBehaviour {

	public Animator damageAnim;
	private Text damageText;

	void OnEnable ()
	{
		AnimatorClipInfo[] clipInfo = damageAnim.GetCurrentAnimatorClipInfo (0);
		Destroy (gameObject, clipInfo [0].clip.length);
		damageText = damageAnim.GetComponent<Text> ();
	}

	public void SetText (string text)
	{
		damageText.text = text;
	}
}
