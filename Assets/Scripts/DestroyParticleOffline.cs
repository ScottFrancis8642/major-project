﻿using UnityEngine;
using System.Collections;

public class DestroyParticleOffline : MonoBehaviour {

	public float waitTime = 2;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine ("DestroyParticle", waitTime);
	}

	// Update is called once per frame
	void Update () 
	{

	}

	IEnumerator DestroyParticle (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);

		Destroy (this.gameObject);
	}
}

