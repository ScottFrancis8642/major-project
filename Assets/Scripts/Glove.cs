﻿using UnityEngine;
using System.Collections;

public class Glove : Photon.MonoBehaviour {

	public ShipController shipScript;
	public string enemyName;
	public GameObject crack;
	public GameObject crackPosition;

	void Update ()
	{
		if (shipScript.punching == true) 
		{
			RaycastHit hit;
			Vector3 fwd = transform.TransformDirection (Vector3.forward);
			Debug.DrawRay (crackPosition.transform.position, transform.forward * 0.3f, Color.green);

			if(Physics.Raycast (crackPosition.transform.position, fwd, out hit, 0.3f))
			{
				Instantiate (crack, hit.point, Quaternion.FromToRotation (Vector3.back, hit.normal));
				UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [3]);

				shipScript.punching = false;
			}



		}



	}


	private void OnTriggerEnter(Collider col)
	{
		

		if (col.gameObject.tag == "Player") 
		{
	        PhotonNetwork.Instantiate ("starParticle", this.transform.position, Quaternion.identity, 0);

			DamageTextController.CreateFloatingText (shipScript.myDamage.ToString (), transform);
	        Metrics.instance.meleePunchesHit++;

			// This is the call to do damage.
	        PhotonView pv = col.transform.root.GetComponent<PhotonView>();
			ShipController myShipC = transform.root.GetComponent<ShipController>();
			pv.RPC ("ReduceHealth", PhotonTargets.All, myShipC.myDamage, PhotonNetwork.player.ID, myShipC.gameObject.GetPhotonView().viewID, myShipC.transform.position, myShipC.hitForce, myShipC.hitForceUp);

          
			shipScript.punching = false;

        }

		//if (col.gameObject.tag == "Ground") 
		//{
		//	Instantiate (crack, col.ClosestPointOnBounds (transform.position), Quaternion.identity);
		//	shipScript.punching = false;
		//}
    }












}
