﻿using UnityEngine;
using System.Collections;

public class GloveRanged : Photon.MonoBehaviour {

	public GameObject myship;
	public float waitTime = 3;



	void Start ()
	{

		StartCoroutine (DestroyGlove (waitTime));
	}

	void Update () 
	{
		 


	}
		
	
	IEnumerator DestroyGlove (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		if(photonView.isMine)
		PhotonNetwork.Destroy (this.gameObject);
	}

	private void OnCollisionEnter(Collision col)
	{
		GameObject sd = (GameObject) PhotonNetwork.Instantiate ("SplashDamage", this.transform.position, Quaternion.identity, 0);
		sd.GetComponent<SplashDamage> ().myship = myship;
		GameObject dp = (GameObject) PhotonNetwork.Instantiate ("GloveExplosion", this.transform.position, Quaternion.identity, 0);
		PhotonNetwork.Destroy (this.gameObject);

		UIManager.instance.BGM.Stop();
		UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [1]);

		/*if (col.gameObject.tag == "Player" && col.gameObject.GetComponent<PhotonView>().ownerId != GetComponent<PhotonView>().ownerId)  // col.transform.root.GetComponent<PhotonView> ().ownerId != player.GetComponent<PhotonView>().ownerId) 
		{
			PhotonNetwork.Instantiate ("starParticle", this.transform.position, Quaternion.identity, 0);
			DamageTextController.CreateFloatingText (myship.myRangedDamage.ToString (), transform);
			Metrics.instance.rangedPunchesHit++;

			Debug.Log (col.gameObject.name);


			// This is to do damage.
			PhotonView pv = col.transform.root.GetComponent<PhotonView> ();
			print (col.transform.root.GetComponent<PhotonView> ().owner.ID);
			pv.RPC ("ReduceHealth", PhotonTargets.All, myship.myRangedDamage,  GetComponent<PhotonView>().ownerId, myship.gameObject.transform.position, myship.hitForce, myship.hitForceUp);


			//UIManager.instance.BGM.clip = UIManager.instance.playerSounds [3];
			//UIManager.instance.BGM.Play ();

		}*/

	} 

}
