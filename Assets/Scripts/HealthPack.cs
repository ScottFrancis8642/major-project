﻿using UnityEngine;
using System.Collections;

public class HealthPack : Photon.MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Player" && col.gameObject.transform.root.GetComponent<PhotonView> ().isMine && col.gameObject.GetComponent<NetworkPlayer>().currentHitPoints < 100) 
			{
			col.gameObject.GetComponent<NetworkPlayer>().currentHitPoints += 100;
				PickUp ();
			if ( col.gameObject.GetComponent<NetworkPlayer>().currentHitPoints >= 100) 
				{
				col.gameObject.GetComponent<NetworkPlayer>().currentHitPoints = 100;
				}
			}
	}	

	void PickUp ()
	{
		photonView.RPC ("DestroyHealthPack", PhotonTargets.MasterClient);
	}

	[PunRPC]
	public void DestroyHealthPack()
	{
		PhotonNetwork.Destroy(this.gameObject);
	}
}
