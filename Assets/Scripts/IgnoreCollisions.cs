﻿using UnityEngine;
using System.Collections;

public class IgnoreCollisions : MonoBehaviour {


	GameObject player;

	// Use this for initialization
	void Awake () 
	{
		//Physics.IgnoreCollision (this.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
			

	}

	void OnCollisionEnter (Collision collision)
	{
		if (collision.gameObject.tag == "Bullet")
		{
			Physics.IgnoreCollision (GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
		}
	}
}
