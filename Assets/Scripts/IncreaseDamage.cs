﻿using UnityEngine;
using System.Collections;

public class IncreaseDamage : PowerUp 
{
	
	private int damage;

	public GameObject increaseKnockbackPUP;

	void Awake()
	{
		powerUpSprite = Resources.Load<Sprite>("PowerUpImages/IncreaseDamageIcon");
		increaseKnockbackPUP = GameObject.Find ("IncreaseKnockbackPUP");
	}
		

	public void Start()
	{
		//Assign my type
		myPowerUpType = powerupType.increaseDamage;
		pTimer = 5;
		//player = GameObject.FindGameObjectWithTag ("Player");
		 
	}

	public override void RunPowerup()
	{
		base.RunPowerup();

		photonView.RPC("TurnOnObject", PhotonTargets.All, "IncreaseKnockbackPUP", true);

		print("doing damage stuff now");

		StartCoroutine(powRunning());

		hasEnded = false;
	}

	public IEnumerator powRunning()
	{
		print("started powerup");
		player.GetComponent<NetworkPlayer> ().knockbackMulti = 3;
		yield return new WaitForSeconds(pTimer);
		player.GetComponent<NetworkPlayer> ().knockbackMulti = 1;
		photonView.RPC("TurnOnObject", PhotonTargets.All, "IncreaseKnockbackPUP", false);
		hasEnded = true;

	}
}
