﻿using UnityEngine;
using System.Collections;

public class KillBox : Photon.MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Player") 
		{
			
			PhotonView pv = col.transform.root.GetComponent<PhotonView> ();

			int tempPlayerLastHit = col.transform.root.GetComponent<NetworkPlayer>().lastHitByEnemyID;
			;
			// Damage functions by RPC
			//pv.RPC ("ReduceHealth", PhotonTargets.All, 100, PhotonNetwork.player.ID);
			pv.RPC ("ReduceHealth", PhotonTargets.All, 100, tempPlayerLastHit, col.transform.root.GetComponent<NetworkPlayer>().enemyViewID, Vector3.zero, 0f, 0f);


		}
	}

}

