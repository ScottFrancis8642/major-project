﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

public class Metrics : MonoBehaviour {

	public static Metrics instance;

	public string PlayerName;
	public int meleePunchesHit = 0;
	public int rangedPunchesHit = 0;
	public float timeBig = 0;
	public float timeSmall = 0;
	public int changedScale = 0;
	public int killsAsBig = 0;
	public int killsAsSmall = 0;
	public int deathsAsBig = 0;
	public int deathsAsSmall = 0;
	public int suicides = 0;
	public int environmentKills = 0;
	public int environmentDeaths = 0;
	public int powerupsUsed = 0;
	public float KingPoints = 0;

	void Awake ()
	{
		instance = this;
	}

	void Update ()
	{
		StartCoroutine (MetricsStats());
	}

	IEnumerator MetricsStats()
	{
		using (StreamWriter writer = File.CreateText(@"C:\Users\Public\DocumentsAnalysis.txt"))
		{
			writer.WriteLine ("WINNER: " + PlayerName);										//0
			writer.WriteLine ("Player: " + PhotonNetwork.player.name + '\n');				//1
			writer.WriteLine ("Glove Kills: " + (meleePunchesHit + rangedPunchesHit)); 		//2
			writer.WriteLine ("Time big: " + timeBig);    									//3
			writer.WriteLine ("Time small: " + timeSmall);									//4
			writer.WriteLine ("Changed scale: " + changedScale);							//5
			writer.WriteLine ("Deaths as big: " + deathsAsBig);								//6
			writer.WriteLine ("Deaths as small: " + deathsAsSmall);							//7
			writer.WriteLine ("Suicides: " + suicides);										//8
			writer.WriteLine ("Environment kills: " + environmentKills);					//9
			writer.WriteLine ("Environment deaths: " + environmentDeaths);					//10
			writer.WriteLine ("Powerups Used: " + powerupsUsed);							//11
			writer.WriteLine ("King Points: " + ((Mathf.RoundToInt(KingPoints)) + "%"));		//12
		}

		yield return null;
	}
}
