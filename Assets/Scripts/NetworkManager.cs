﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour 
{
	[Header("GameObjects")]
	[SerializeField] GameObject[] spawnPoints;
	[SerializeField] GameObject[] spawnCheckers;
	[SerializeField] GameObject roomPrefab;
	[SerializeField] GameObject myCamera;
	[SerializeField] GameObject connectManualButton;
	[SerializeField] GameObject connectIPconfigIF;
	[SerializeField] GameObject connectAutomaticButton;

	GameObject player;
	public GameObject cp;

	[Header("Player Screen Properties")]
	[SerializeField] GameObject serverWindow;
	[SerializeField] public GameObject playerHUDWindow;
	[SerializeField] public GameObject playerDeathWindow;
	[SerializeField] InputField userName;
	[SerializeField] InputField IPConfigIF;
	[SerializeField] InputField roomNameText;
	[SerializeField] InputField messageWindow;
	[SerializeField] int maxPlayers = 4;

	[Header("Lists")]
	private List<GameObject> roomPrefabs = new List<GameObject> ();
	private List<GameObject> playerNamesPrefabs = new List<GameObject> ();

	[Header("Message Box / Player Feed")]
	Queue<string> messages;
	const int messageCount = 6;
	PhotonView photonView;

	NetworkPlayer np;
	bool gameReady = false;
	bool roomCreated = false;
	float kingPoints;
	public bool BeginningSpawn;
	int playerSpawn;

	public enum ServerMode
	{
		MANUALLY,
		AUTOMATIC
	};

	public ServerMode currentServerMode;

		void Awake()
	{
		GetComponent<ScoreManager> ().enabled = false;
		messageWindow.gameObject.SetActive (false);
		BeginningSpawn = true;
	}

	void Start () 
	{
		photonView = GetComponent<PhotonView> ();
		messages = new Queue<string> (messageCount);
		currentServerMode = ServerMode.MANUALLY;

		// Only show us errors during connection.
		PhotonNetwork.logLevel = PhotonLogLevel.ErrorsOnly;

		// Connect automatically to the lobby as connecting to master will not call this.
		PhotonNetwork.autoJoinLobby = true;
	}

	public void ChangeServerMode()
	{
		if (currentServerMode == ServerMode.MANUALLY)
			currentServerMode = ServerMode.AUTOMATIC;
		else if (currentServerMode == ServerMode.AUTOMATIC)
			currentServerMode = ServerMode.MANUALLY;
	}

	public void Connect()
	{
		// Connect manually through IP configuration set by the local user.
		PhotonNetwork.ConnectToMaster (IPConfigIF.text, 5055, "b8be7433-ad2b-45ed-a2cd-d93942be6743", "0.2");
	}

	public void ConnectAutomatically()
	{
		// This is automatically connects via the editor settings / Used for photon cloud server
		PhotonNetwork.ConnectUsingSettings ("0.2");
	}

	void Update()
	{
		switch (currentServerMode)
		{
		case ServerMode.AUTOMATIC:
			connectManualButton.SetActive (false);
			connectIPconfigIF.SetActive (false);
			connectAutomaticButton.SetActive (true);
			break;
		case ServerMode.MANUALLY:
			connectManualButton.SetActive (true);
			connectIPconfigIF.SetActive (true);
			connectAutomaticButton.SetActive (false);
			break;
		}
	}

	/// <summary>
	/// Raises the joined lobby event.
	/// </summary>
	void OnJoinedLobby()
	{
		serverWindow.SetActive (true);
		UIManager.instance.BGM.Play ();

		// Refresh the room list in a short time frame for the refresh button
		Invoke ("RefreshRoomList", 0.1f);

		// The refresh function to look up our rooms in the list.
		RefreshRoomList ();
		//Invoke ("DisplayPlayerNames", 5);

	}


	/// <summary>
	/// Shows the player list.
	/// </summary>


	/// <summary>
	/// Refreshes the room list.
	/// </summary>
	void RefreshRoomList()
	{
		if (roomPrefabs.Count > 0) 
		{
			for (int i = 0; i < roomPrefabs.Count; i++) 
			{
				Destroy (roomPrefabs [i]);	
			}

			roomPrefabs.Clear ();
		}

		for (int i = 0; i < PhotonNetwork.GetRoomList ().Length; i++) 
		{
			if (PhotonNetwork.GetRoomList () [i].name.Contains ("PlanetChasers")) 
			{
				
				Debug.Log (PhotonNetwork.GetRoomList () [i].name);

				// For every room created, instantiate a new room entry
				GameObject g = Instantiate (roomPrefab);

				// Set the parent of the roomPrefab to follow under
				g.transform.SetParent (roomPrefab.transform.parent);

				// Follow it's local scale so it doesn't pop out of the border
				g.GetComponent<RectTransform> ().localScale = roomPrefab.GetComponent<RectTransform> ().localScale;

				// Follow it's local position so it doesn't pop out of the border
				g.GetComponent<RectTransform> ().localPosition = new Vector3 (roomPrefab.GetComponent<RectTransform> ().localPosition.x, roomPrefab.GetComponent<RectTransform> ().localPosition.y - (i * 50), roomPrefab.GetComponent<RectTransform> ().localPosition.z);

				string planetChasersNameHolder = PhotonNetwork.GetRoomList () [i].name;

				// Find the child of room name text
				g.transform.FindChild ("roomNameLabel").GetComponent<Text> ().text = planetChasersNameHolder.Remove (0, 13);

				Debug.Log (g.transform.FindChild ("roomNameLabel").GetComponent<Text> ().text);

				// Find the child of room name text
				g.transform.FindChild ("numberOfPlayers").GetComponent<Text> ().text = PhotonNetwork.GetRoomList () [i].playerCount + " / " + maxPlayers;

				// Find the join button.
				g.transform.FindChild ("joinBTN").GetComponent<Button> ().onClick.AddListener (() => {
					JoinRoom ("PlanetChasers" + g.transform.FindChild ("roomNameLabel").GetComponent<Text> ().text);
				});

				// Make it active in our lobby.
				g.SetActive (true);

				roomPrefabs.Add (g);
			}
		}
	}

	/// <summary>
	/// Creates the button events.
	/// </summary>
	/// <param name="EVENT">Name of the events being called by case switch statement </param>
	public void ButtonEvents(string EVENT)
	{
		switch (EVENT) 
		{
		case "CreateUserName":
			/// Create the username
			PhotonNetwork.player.name = userName.text;
			break;
		case "CreateRoom":
			RoomOptions ro = new RoomOptions (){ isVisible = true, maxPlayers = 4 };
			PhotonNetwork.CreateRoom ("PlanetChasers" + roomNameText.text, ro, TypedLobby.Default);
			break;
		case "LeaveCurrentRoom":
			PhotonNetwork.Disconnect ();
			Debug.Log ("Leaving room...");
			PhotonNetwork.Reconnect ();
			break;
		case "Refresh":
			RefreshRoomList ();
			break;
		}
	}


	/// <summary>
	/// Joins the room.
	/// </summary>
	/// <param name="roomName">Room name.</param>
	public void JoinRoom(string roomName)
	{
		bool availableRoom = false;
		foreach (RoomInfo RI in PhotonNetwork.GetRoomList()) 
		{
			if (roomName == RI.name) 
			{
				availableRoom = true;
				break;
			} 
			else 
			{
				availableRoom = false;
			}
		}

		if (availableRoom) {
			// Join the game or create the game based off what we named it.
			PhotonNetwork.JoinRoom (roomName);

		} 
		else
		{
			Debug.LogError ("Room unavailable");
		}


		// Enable the score manager.
		//GetComponent<ScoreManager> ().enabled = true;
		gameObject.GetComponent<AudioListener> ().enabled = false;
		UIManager.instance.BGM.Stop ();

	}
	
	/// <summary>
	/// Raises the photon join room failed event.
	/// </summary>
	void OnPhotonJoinRoomFailed()
	{
		Debug.Log ("Failed to join room");
	}

	void OnJoinedRoom()
	{
		if(PhotonNetwork.isMasterClient)
		{
			UIManager.instance.JoinGameBTN.SetActive (true);
		}

		UIManager.instance.panels [0].SetActive (true);
		UIManager.instance.panels [1].SetActive (true);
		UIManager.instance.panels [2].SetActive (true);
		UIManager.instance.panels [3].SetActive (true);
	}

	/// <summary>
	/// Raises the win condition.
	/// </summary>
	[PunRPC]
	public void WinCondition()
	{
		SceneManager.LoadScene ("ScoreboardScreen");
		playerHUDWindow.SetActive (false);
	}

	/// <summary>
	/// Starts the spawn process.
	/// </summary>
	/// <param name="respawnTime">Respawn time.</param>
	public void StartSpawnProcess (float respawnTime)
	{
		myCamera.SetActive (true);
		StartCoroutine ("SpawnPlayer", respawnTime);
	}

	/// <summary>
	/// Spawns the player.
	/// </summary>
	/// <returns>The player.</returns>
	/// <param name="respawnTime">Respawn time.</param>
	IEnumerator SpawnPlayer(float respawnTime)
	{
        yield return new WaitForSeconds(respawnTime);

      	player = PhotonNetwork.Instantiate ("Player", ChooseAvailableSpawnPoint(), Quaternion.identity, 0);
		player.GetComponent<NetworkPlayer>().controlPoints = PlayerPrefs.GetFloat ("ControlPoints");
		player.GetComponent<NetworkPlayer> ().kingPoints = PlayerPrefs.GetFloat ("KingPoints");

        player.GetComponent<ShipController>().enabled = true;
        player.GetComponent<NetworkPlayer> ().RespawnMe += StartSpawnProcess;
		player.GetComponent<NetworkPlayer> ().SendNetworkMessage += AddMessage;

		// Disable our cameras
		myCamera.SetActive (false);
		UIManager.instance.standbyCamera.SetActive (false);
		UIManager.instance.levelViewStandbyCamera.SetActive (false);

		//AddMessage ("Spawned player: " + PhotonNetwork.player.name);
		UIManager.instance.powerUpIcon.SetActive (false);
		BeginningSpawn = false;

	}

	/// <summary>
	/// Adds the message.
	/// </summary>
	/// <param name="message">Message.</param>
	void AddMessage(string message)
	{
		photonView.RPC ("AddMessage_RPC", PhotonTargets.All, message);
	}

	[PunRPC]
	void StartGame()
	{
		serverWindow.SetActive (false);
		playerHUDWindow.SetActive (true);
		messageWindow.gameObject.SetActive (true);
		gameObject.GetComponent<AudioListener> ().enabled = false;
		UIManager.instance.BGM.Stop ();
		PlayerPrefs.DeleteAll ();
		StartSpawnProcess (0f);
		UIManager.instance.BGM.Play ();
	}
	/// <summary>
	/// Adds the message via RPC.
	/// </summary>
	/// <param name="message">The Message.</param>
	[PunRPC]
	void AddMessage_RPC(string message)
	{
		messages.Enqueue (message);
		if (messages.Count > messageCount) 
		{
			messages.Dequeue ();
		}

		messageWindow.text = "";
		foreach (string m in messages) 
		{
			messageWindow.text += m + "\n";
		}

	}

	/// <summary>
	/// Raises the photon player connected event.
	/// </summary>
	/// <param name="p">P.</param>
	public void OnPhotonPlayerConnected(PhotonPlayer p)
	{
		AddMessage (p.name + " has joined the game.");
	}

	/// <summary>
	/// Raises the photon player disconnected event.
	/// </summary>
	/// <param name="p">P.</param>
	public void OnPhotonPlayerDisconnected(PhotonPlayer p)
	{
		AddMessage (p.name + " has left the game.");
	}  

	/// <summary>
	/// Chooses the available spawn point.
	/// </summary>
	/// <returns>The available spawn point.</returns>
	Vector3 ChooseAvailableSpawnPoint ()
	{
		if (BeginningSpawn == true) 
		{
			if (PhotonNetwork.player.ID == 1) 
			{
				return spawnCheckers [0].GetComponent<SpawnChecker> ().mySpawn.transform.position;
			}

			if (PhotonNetwork.player.ID == 2) 
			{
				return spawnCheckers [1].GetComponent<SpawnChecker> ().mySpawn.transform.position;
			}

			if (PhotonNetwork.player.ID == 3) 
			{
				return spawnCheckers [2].GetComponent<SpawnChecker> ().mySpawn.transform.position;
			}

			if (PhotonNetwork.player.ID == 4)
			{
				return spawnCheckers [3].GetComponent<SpawnChecker> ().mySpawn.transform.position;
			}
			return spawnCheckers [0].GetComponent<SpawnChecker> ().mySpawn.transform.position;
		}

		else
		{
			foreach (GameObject sc in spawnCheckers) 
			{
				if (!sc.GetComponent<SpawnChecker> ().playerInSpawn) 
				{
					return sc.GetComponent<SpawnChecker> ().mySpawn.transform.position;
				}
			}
			return spawnCheckers [0].transform.position;
		}
	}

	public void StartGameButton()
	{
		//Call the startgame RPC on the Network manager on all clients\
		PhotonNetwork.RPC (gameObject.GetPhotonView (), "StartGame", PhotonTargets.AllBufferedViaServer,false);
		BeginningSpawn = true;
	}
}