﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NetworkPlayer : Photon.MonoBehaviour
{
	public delegate void Respawn(float time);
	public event Respawn RespawnMe;

	public delegate void SendMessage(string MessageOverlay);
	public event SendMessage SendNetworkMessage;

	[Header("Camera")]
	public Camera myCamera;
	public Camera radarCamera;
	public Camera secondCam;
	public GameObject radarImage;
	public NetworkManager netMan;

	[Header("Ship status and transform stats")]
	bool isAlive = true;

	Vector3 position = Vector3.zero;
	Quaternion rotation = Quaternion.identity;
	float lerpSmoothing = 0.01f;

	[Header("Ship GameObjects")]
	public GameObject thrusterParticle;

	public GameObject[] shipModels;
	public GameObject[] secondCamModels;

	public GameObject shipRoot;
	public GameObject playerPosition;
	public GameObject glovePosition;

	public GameObject GameManager;

	[Header("Ship Player HUD")]
	public Sprite[] redShipHud;
	public Sprite[] blueShipHud;
	public Sprite[] blackShipHud;
	public Sprite[] yellowShipHud;

	[Header("Ship Particle Systems")]
	public ParticleSystem speedBoostPUP;
	public ParticleSystem rapidPunchPUP;
	public ParticleSystem superJumpPUP;
	public ParticleSystem increaseKnockbackPUP;


	[Header("Player ID and enemy ID")]
	public int lastHitByEnemyID;
	public int enemyViewID;

	[Header("Animations")]
	private Animator punchAnim;

	[Header("Timers")]
	public float lastHitTimer = 8f;

	[Header("Player Stats")]
	// Declare the player health here
	public int maxPlayerHitPoints = 100;
	public int currentHitPoints;
	public float knockbackMulti;

	[Header("Control point settings")]
	public float controlPoints;
	public float maxControlPoints = 100f;
	public GameObject controlPoint;
	public GameObject crown;

	float waitTime = 0.2f;
	public bool isKing = false;

	public float kingPoints;
	public float maxKingPoints = 100;

	[Header("Ship Colours")]
	public GameObject shipColourScript;
	public Material[] colorRedMaterials;
	public Material[] colorBlueMaterials;
	public Material[] colorBlackMaterials;
	public Material[] colorYellowMaterials;

	public bool warning;

	void Awake ()
	{
		shipColourScript = GameObject.FindGameObjectWithTag ("Canvas");

	}

	void Start()
	{
		// Declare the currentHitPoints.
		currentHitPoints = maxPlayerHitPoints;
		knockbackMulti = 1;
		controlPoint = GameObject.FindGameObjectWithTag("ControlPoint");
		crown = controlPoint.GetComponent<ControlPoint> ().crownCopy;
		netMan = GameObject.FindObjectOfType<NetworkManager>();
		netMan.playerHUDWindow.SetActive (true);
		GameManager = GameObject.Find ("GameManager");

		if (photonView.isMine) {

			gameObject.name = "DefaultPlayer";
			FindObjectOfType<ControlPoint> ().me = this.gameObject;
			myCamera.enabled = true;
			radarCamera.enabled = true;
			secondCam.enabled = true;
			GetComponent<ShipController> ().enabled = true;
			GetComponent<Rigidbody> ().useGravity = true;
			GetComponentInChildren<CamMouseLook> ().enabled = true;
			GetComponent<PlayerPowerUps> ().enabled = true;
			//GetComponent<Tracker>().enabled = true;
			GetComponent<AudioSource> ().enabled = true;
			GetComponent<AnimationManager> ().enabled = true;
			GetComponent<Animator> ().enabled = true;
			playerPosition.SetActive (true);
			playerPosition.GetComponent<Renderer> ().material.color = Color.blue;
			netMan.playerDeathWindow.SetActive (false);

			radarImage.layer = LayerMask.NameToLayer ("LocalRadar");
			radarCamera.cullingMask = (1 << 11) | (1 << 13);
			shipRoot.layer = LayerMask.NameToLayer ("LocalShip");

			// Spectator Mode
			if (UIManager.instance.SpectatorMode == true) 
			{
				netMan.playerHUDWindow.SetActive (false);
				foreach (GameObject child in shipModels) 
				{
					child.GetComponent<Renderer> ().enabled = false;
				}
				foreach (GameObject child in secondCamModels) 
				{
					child.GetComponent<Renderer> ().enabled = false;
				}

			} 
			// Normal Mode
			else
			{
				foreach (GameObject child in shipModels) 
				{
					child.layer = LayerMask.NameToLayer ("LocalShip");
				}
				foreach (GameObject child in secondCamModels)
				{
					child.layer = LayerMask.NameToLayer("Glove");
				}
			}
		

			thrusterParticle.layer = LayerMask.NameToLayer("LocalShip");
			myCamera.cullingMask = (1 << 9) | (1 << 0) | (1 << 4) | (1 << 14);
			secondCam.cullingMask = (1 << 15);

			//setup camera 

			DamageTextController.Initialise();
			DamageTextController.playercam = myCamera;

			//Finally update our color
			this.photonView.RPC("ChangeShipColour",PhotonTargets.AllBuffered,PhotonNetwork.player.ID-1);
		}

		else
		{
			gameObject.name = "Network Player";
			radarImage.layer = LayerMask.NameToLayer("NetworkRadar");
			radarCamera.cullingMask = (1 << 12) | (1 << 13) | (1 << 4);
			//this.photonView.RPC("ChangeShipColour", PhotonTargets.OthersBuffered);

			foreach (GameObject child in shipModels)
			{
				child.layer = LayerMask.NameToLayer("NetworkShip");
			}

			foreach (GameObject child in secondCamModels)
			{
				child.layer = LayerMask.NameToLayer("Glove2");
			}


			thrusterParticle.layer = LayerMask.NameToLayer("NetworkShip");
			playerPosition.SetActive(true);
			playerPosition.GetComponent<Renderer>().material.color = Color.red;
			myCamera.cullingMask = (1 << 8) | (1 << 0) | (1 << 14) | (1 << 4);
			secondCam.cullingMask = (1 << 17);
		}
	}



	void Update()
	{
		LastHitTimer();
		UpdatePlayerHealthSprite();
		IsKing();

		//Test if scoreboard works
//		if (Input.GetKeyDown (KeyCode.P)) 
//		{
//			kingPoints = maxKingPoints;
//		}
//		// FOR TESTING PURPORSES

		if (photonView.isMine)
		{
			

			UIManager.instance.controlPoints.text = "Control Points: " + ((int)controlPoints).ToString() + "%";
			UIManager.instance.kingPoints.text = "King Points: " +((int)kingPoints).ToString() + "%";
			UIManager.instance.healthText.text = "Health: " + ((int)currentHitPoints).ToString () + "%";


			if (currentHitPoints <= 30 || warning == true) 
			{
				UIManager.instance.warningGlow.enabled = true;
				UIManager.instance.cracks.enabled = true;

			} 
			else 
			{
				UIManager.instance.warningGlow.enabled = false;
				UIManager.instance.cracks.enabled = false;

			}

		}
		else
		{
			transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * lerpSmoothing);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * lerpSmoothing);
		}

	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(thrusterParticle);
			stream.SendNext(currentHitPoints);
			stream.SendNext(controlPoints);
			stream.SendNext (crown);

		}
		else
		{
			position = (Vector3)stream.ReceiveNext();
			rotation = (Quaternion)stream.ReceiveNext();

		}
	}

	// Last hit timer refers to EName if empty or not.
	void LastHitTimer()
	{
		if (lastHitByEnemyID != 0)
		{
			lastHitTimer -= Time.deltaTime;
			if (lastHitTimer <= 0)
			{
				lastHitByEnemyID = 0;
				lastHitTimer = ConstantsData.LAST_HIT_TIMER;
			}
		}
	}

	void IsKing()
	{
		if (isKing == true) 
		{
			controlPoint.GetComponentInChildren<crownedKingScript> ().cannotTake = true;
			controlPoint.GetComponentInChildren<crownedKingScript> ().playerWhoIsKing = this.gameObject;
				
			if (photonView.isMine)
			{
				kingPoints += Time.deltaTime;
				Metrics.instance.KingPoints = kingPoints;
				UIManager.instance.controlPointsRadial.fillAmount = kingPoints / 100;
				UIManager.instance.KingCrownImage.enabled = true;
			}

		}

		if (kingPoints >= maxKingPoints) 
		{
			kingPoints = maxKingPoints;
			GameManager.GetComponent<PhotonView> ().RPC ("WinCondition", PhotonTargets.All);
		}

	}


	void UpdatePlayerHealthSprite()
	{
		if (photonView.isMine)
		{
			if (currentHitPoints == 100)
			{
				// Starting health bar
				UIManager.instance.playerHealthBar.fillAmount = 1;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.green;
			}
			if (currentHitPoints >= 90 && currentHitPoints < 100)
			{
				// 90%
				UIManager.instance.playerHealthBar.fillAmount = 0.9f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.green;
			}
			if (currentHitPoints >= 80 && currentHitPoints < 90)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.8f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.green;
			}
			if (currentHitPoints >= 70 && currentHitPoints < 80)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.7f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.green;
			}
			if (currentHitPoints >= 60 && currentHitPoints < 70)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.6f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.green;	
			}
			if (currentHitPoints >= 50 && currentHitPoints < 60)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.5f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.yellow;
			}
			if (currentHitPoints >= 40 && currentHitPoints < 50)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.4f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.yellow;
			}
			if (currentHitPoints >= 30 && currentHitPoints < 40)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.3f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.yellow;
			}
			if (currentHitPoints >= 20 && currentHitPoints < 30)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.2f;
				UIManager.instance.playerHealthBar.GetComponent<Image> ().color = Color.red;
			}
			if (currentHitPoints >= 10 && currentHitPoints < 20)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0.1f;
				UIManager.instance.playerHealthBar.color = Color.red;
			}
			if (currentHitPoints > 0 && currentHitPoints < 10)
			{
				UIManager.instance.playerHealthBar.fillAmount = 0;
				UIManager.instance.playerHealthBar.color = Color.red;
			}
		}
	}

	//[PunRPC]
	public void IncreaseControlPoints()
	{
		
			controlPoints += Time.deltaTime * 10;
			
			if (controlPoints >= maxControlPoints) 
			{
				controlPoints = maxControlPoints;
				UIManager.instance.controlPointsRadial.fillAmount = 0;
				//photonView.RPC ("TurnOnIsKing", PhotonTargets.All);
				controlPoint.GetComponentInChildren<crownedKingScript> ().playerWhoIsKing = this.gameObject;
				this.photonView.RPC("TurnOnIsKing", PhotonTargets.All);
				
			}

	}

	[PunRPC]
	void TurnOnObject(string n, bool v)
	{
		transform.FindChild(n).gameObject.SetActive(v);
	}

	[PunRPC]
	void TurnOnIsKing ()
	{
		GetComponent<NetworkPlayer> ().isKing = true;                          
		crown.GetComponentInChildren<MeshRenderer> ().enabled = true;
		crown.GetComponent<SphereCollider> ().enabled = true;
		controlPoint.GetComponent<ControlPoint> ().enabled = false;
		controlPoint.GetComponent<ControlPoint> ().controlPointPS.gameObject.SetActive (false);
		UIManager.instance.PlayersInCp.enabled = false;
		UIManager.instance.PlayersInCpNumber.enabled = false;
		UIManager.instance.controlPoints.enabled = false;
		UIManager.instance.kingPoints.enabled = true;
	}



	/// <summary>
	/// Reduces the health.
	/// </summary>
	/// <param name="damage">The damage done to us or other players.</param>
	/// <param name="enemyID">The enemy player's ID.</param>
	/// <param name="enemyPos">The enemy player's position.</param>
	/// <param name="enemyHitForce">The enemy's forward hit force.</param>
	/// <param name="enemyHitForceUp">The enemy's upward hit force.</param>
	[PunRPC]
	public void ReduceHealth(int damage, int enemyID, int enemyViewId, Vector3 enemyPos, float enemyHitForce, float enemyHitForceUp)
	{				// If the client is ours.
		if (photonView.isMine)
		{
			// Only reduce health if we're alive.
			if (isAlive)
			{
				// Restart the last hit timer for a new player that hit it.
				lastHitTimer = ConstantsData.LAST_HIT_TIMER;

				// Take away damage from our player health.
				currentHitPoints -= damage;
				Debug.Log("Player Hit: " + damage);
				StartCoroutine (CameraShake (0.35f));

				// print the enemy name on EName
				print(enemyID);
				lastHitByEnemyID = enemyID;
				enemyViewID = enemyViewId;
				



				// Add the force of up and forward for knockback feedback.
				gameObject.GetComponent<Rigidbody>().AddForce(Vector3.Normalize(transform.root.position - enemyPos) * enemyHitForce * knockbackMulti, ForceMode.Force);
				gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * enemyHitForceUp * knockbackMulti);

				// Play the sound when being attacked.
				UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [3]);


				// Destroy our player after we go below 0.
				if (currentHitPoints <= 0 && isAlive)
				{
					//Im dead now
					isAlive = false;

					// Turn off king status if player is king
					UIManager.instance.KingCrownImage.enabled = false;

					//Turn off playerHUD as well
					netMan.playerHUDWindow.SetActive(false);

					// Turn on player death window
					netMan.playerDeathWindow.SetActive (true);

					// Set our hit points to 0.
					currentHitPoints = 0;

					// Respawn our player.
					if (RespawnMe != null)
					{
						// Respawn our player after 5 seconds.
						RespawnMe(5f);
					}

					if (lastHitByEnemyID != PhotonNetwork.player.ID && lastHitByEnemyID != 0)
					{
						PhotonPlayer.Find(lastHitByEnemyID).AddScore(1);
	
						if(isKing == true)
						{
							PhotonView.Find(enemyViewId).gameObject.GetPhotonView().RPC("TurnOnIsKing", PhotonTargets.All);
							isKing = false;
						}
						if (SendNetworkMessage != null && lastHitByEnemyID != 0) 
						{
							SendNetworkMessage (PhotonNetwork.player.name + " was killed by " + PhotonPlayer.Find (lastHitByEnemyID).name); 
							Metrics.instance.environmentKills++;
						}
					}
					else if (lastHitByEnemyID == PhotonNetwork.player.ID || lastHitByEnemyID == 0)
					{
						if (SendNetworkMessage != null) 
						{
							SendNetworkMessage (PhotonNetwork.player.name + " fell off the edge...");
							Metrics.instance.environmentDeaths++;
						}

						if (isKing == true) 
						{
							Debug.Log ("Suicide as King");
							crown.GetPhotonView ().RPC ("FreeCrown", PhotonTargets.All);
						} 
					}

					PlayerPrefs.SetFloat("ControlPoints", controlPoints);
					PlayerPrefs.SetFloat ("KingPoints", kingPoints);
					controlPoint.GetComponent<ControlPoint>().playersInRange.Remove(this.GetComponent<NetworkPlayer>());
					
					PhotonNetwork.Instantiate("DeathParticle", this.transform.position, Quaternion.identity, 0);

					UIManager.instance.levelViewStandbyCamera.SetActive (true);
					// Destroy the object by network.
					PhotonNetwork.Destroy(gameObject);
				}
			}
		}
	}
		
	IEnumerator CameraShake (float waitTime)
	{
		myCamera.GetComponent<CameraShake> ().enabled = true;
		myCamera.GetComponent<CameraShake> ().shakeDuration = 0.35f;
		warning = true;
		yield return new WaitForSeconds (waitTime);
		myCamera.GetComponent<CameraShake> ().enabled = false;
		warning = false;
	}

	[PunRPC]
	void ChangeShipColour (int ppNum)
	{
		if (Customization.instance.playerColors[ppNum].Equals(0)) 
		{
			if (photonView.isMine) {
				Customization.instance.playerHUDBGIconPlaceHolders [0].sprite = redShipHud [0];
				Customization.instance.playerHUDBGIconPlaceHolders [1].sprite = redShipHud [1];
				Customization.instance.playerHUDBGIconPlaceHolders [2].sprite = redShipHud [2];
				GetComponent<ShipController> ().smallShip = redShipHud [3];
				GetComponent<ShipController> ().bigShip = redShipHud [0];
			}

			shipModels [0].GetComponent<Renderer> ().material = colorRedMaterials [0];
			shipModels [1].GetComponent<Renderer> ().material = colorRedMaterials [1];
			shipModels [2].GetComponent<Renderer> ().material = colorRedMaterials [2];
			shipModels [3].GetComponent<Renderer> ().material = colorRedMaterials [3];
			shipModels [10].GetComponent<Renderer> ().material = colorRedMaterials [4];

			secondCamModels[0].GetComponent<Renderer> ().material = colorRedMaterials [0];
			secondCamModels[1].GetComponent<Renderer> ().material = colorRedMaterials [4];
		}

		else if (Customization.instance.playerColors[ppNum].Equals(1)) 
		{
			if (photonView.isMine) {
				Customization.instance.playerHUDBGIconPlaceHolders [0].sprite = blueShipHud [0];
				Customization.instance.playerHUDBGIconPlaceHolders [1].sprite = blueShipHud [1];
				Customization.instance.playerHUDBGIconPlaceHolders [2].sprite = blueShipHud [2];
				GetComponent<ShipController> ().smallShip = blueShipHud [3];
				GetComponent<ShipController> ().bigShip = blueShipHud [0];
			}
			shipModels [0].GetComponent<Renderer> ().sharedMaterial = colorBlueMaterials [0];
			shipModels [1].GetComponent<Renderer> ().sharedMaterial = colorBlueMaterials [1];
			shipModels [2].GetComponent<Renderer> ().sharedMaterial = colorBlueMaterials [2];
			shipModels [3].GetComponent<Renderer> ().sharedMaterial = colorBlueMaterials [3];
			shipModels [10].GetComponent<Renderer> ().sharedMaterial = colorBlueMaterials [4];
		
			secondCamModels[0].GetComponent<Renderer> ().sharedMaterial = colorBlueMaterials [0];
			secondCamModels[1].GetComponent<Renderer> ().sharedMaterial = colorBlueMaterials [4];
		}

		else if (Customization.instance.playerColors[ppNum].Equals(2)) 
		{
			if (photonView.isMine) {
				Customization.instance.playerHUDBGIconPlaceHolders [0].sprite = blackShipHud [0];
				Customization.instance.playerHUDBGIconPlaceHolders [1].sprite = blackShipHud [1];
				Customization.instance.playerHUDBGIconPlaceHolders [2].sprite = blackShipHud [2];
				GetComponent<ShipController> ().smallShip = blackShipHud [3];
				GetComponent<ShipController> ().bigShip = blackShipHud [0];
			}
			shipModels [0].GetComponent<Renderer> ().material = colorBlackMaterials [0];
			shipModels [1].GetComponent<Renderer> ().material = colorBlackMaterials [1];
			shipModels [2].GetComponent<Renderer> ().material = colorBlackMaterials [2];
			shipModels [3].GetComponent<Renderer> ().material = colorBlackMaterials [3];
			shipModels [10].GetComponent<Renderer> ().material = colorBlackMaterials [4];
		
			secondCamModels[0].GetComponent<Renderer> ().material = colorBlackMaterials [0];
			secondCamModels[1].GetComponent<Renderer> ().material = colorBlackMaterials [4];
		}

		else if (Customization.instance.playerColors[ppNum].Equals(3)) 
		{
			if (photonView.isMine) {
				Customization.instance.playerHUDBGIconPlaceHolders [0].sprite = yellowShipHud [0];
				Customization.instance.playerHUDBGIconPlaceHolders [1].sprite = yellowShipHud [1];
				Customization.instance.playerHUDBGIconPlaceHolders [2].sprite = yellowShipHud [2];
				GetComponent<ShipController> ().smallShip = yellowShipHud [3];
				GetComponent<ShipController> ().bigShip = yellowShipHud [0];
			}
			shipModels [0].GetComponent<Renderer> ().material = colorYellowMaterials [0];
			shipModels [1].GetComponent<Renderer> ().material = colorYellowMaterials [1];
			shipModels [2].GetComponent<Renderer> ().material = colorYellowMaterials [2];
			shipModels [3].GetComponent<Renderer> ().material = colorYellowMaterials [3];
			shipModels [10].GetComponent<Renderer> ().material = colorYellowMaterials [4];
		
			secondCamModels[0].GetComponent<Renderer> ().material = colorYellowMaterials [0];
			secondCamModels[1].GetComponent<Renderer> ().material = colorYellowMaterials [4];
		}

	}
}