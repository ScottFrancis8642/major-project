﻿using UnityEngine;
using System.Collections;

public class ParticleDestroy : Photon.MonoBehaviour {

	public float waitTime = 2;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine ("DestroyParticle", waitTime);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	IEnumerator DestroyParticle (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		if(photonView.isMine)
		PhotonNetwork.Destroy (this.gameObject);
	}
}
