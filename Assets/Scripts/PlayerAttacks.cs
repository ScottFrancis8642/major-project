﻿using UnityEngine;
using System.Collections;

public class PlayerAttacks : Photon.MonoBehaviour
{
	public float fireRate = 0.1f;
	float cooldown = 0f;

	void Update()
	{
		if (Input.GetButton ("Fire1")) 
		{
			Fire ();
		}
	}

	void Fire()
	{
		if (cooldown > 0) 
		{
			return;
		}

		Debug.Log ("Firing our gun! ");

		Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
		RaycastHit hitInfo;

		if (Physics.Raycast(ray, out hitInfo));
		cooldown = fireRate;
	}
}
