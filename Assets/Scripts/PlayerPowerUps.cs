﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerPowerUps : MonoBehaviour {

    public PowerUp myStoredPowerUp;
    public PowerUp myCurrentPowerUp;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(myCurrentPowerUp != null)
        {
            if (myCurrentPowerUp.hasEnded)
            {
				GetComponent<ShipController>().hasPowerUp = false;
                myCurrentPowerUp.PowerUpFinish();
            }
        }
	}

    public void PickUpNewPowerUp(PowerUp.powerupType pUP)
    {
        if (pUP == PowerUp.powerupType.speedBoost)
        {
            SpeedBoost sb = gameObject.AddComponent<SpeedBoost>();
            sb.init();
            myStoredPowerUp = sb;

		}
        if (pUP == PowerUp.powerupType.increaseDamage)
        {
			IncreaseDamage id = gameObject.AddComponent<IncreaseDamage> ();
			id.init ();
			myStoredPowerUp = id;
        }
		if (pUP == PowerUp.powerupType.rapidPunch)
		{
			RapidPunch rp = gameObject.AddComponent<RapidPunch> ();
			rp.init ();
			myStoredPowerUp = rp;
		}
		if (pUP == PowerUp.powerupType.superJump)
		{
			SuperJump sj = gameObject.AddComponent<SuperJump> ();
			sj.init ();
			myStoredPowerUp = sj;
		}

		UIManager.instance.powerUpIcon.GetComponent<Image>().sprite = myStoredPowerUp.powerUpSprite;
    }

    public void RunPowerUp()
    {
		myCurrentPowerUp = myStoredPowerUp;
        myStoredPowerUp = null;
        myCurrentPowerUp.RunPowerup();
    }
}
