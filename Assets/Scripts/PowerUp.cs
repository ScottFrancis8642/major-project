﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerUp : Photon.MonoBehaviour {

    public enum powerupType {speedBoost, increaseDamage, rapidPunch, superJump};
    public powerupType myPowerUpType;
	public Sprite powerUpSprite;

    public float pTimer;
	public bool hasEnded;

	public PowerUpManager powerupManagerScript;

    public ShipController myShip;

	public GameObject player;
    public bool pickedUp;

    //Effects
    [SerializeField]
    private float rotateSpeed = 1.0f;
    [SerializeField]
    private float floatSpeed = 0.5f;
    [SerializeField]
    private float movementDistance = 0.5f;
    [SerializeField]
    private GameObject collectPowerUpEffect;

    private float startingY;
    private bool isMovingUp = true;

	//public bool hasPowerUp;
    
    void Start ()
	{
		//player = GameObject.FindGameObjectWithTag ("Player");
	    startingY = transform.position.y;
        pickedUp = false;

		player.GetComponent<ShipController>().hasPowerUp = false;

    }
    
	void Update ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
        
	}


    #region PrePickUp

    void Float()
    {
        float newY = transform.position.y + (isMovingUp ? 1 : -1) * 2 * movementDistance * floatSpeed * Time.deltaTime;

        if (newY > startingY + movementDistance)
        {
            newY = startingY + movementDistance;
            isMovingUp = false;
        }
        else if (newY < startingY)
        {
            newY = startingY;
            isMovingUp = true;
        }

        transform.position = new Vector3(transform.position.x, newY, transform.position.z);
    }

    //Pick up stuff
    void OnTriggerEnter(Collider collider)
    {
        print(collider.gameObject.name);
       // if (collider.gameObject.transform.root.GetComponent<PhotonView>().isMine)
        //{
		//if ()// && player.GetComponent<PlayerPowerUps>().myStoredPowerUp == null)// && player.GetComponent<PlayerPowerUps>().myCurrentPowerUp == null) 
		//{
			if (collider.gameObject.tag == "Player" && collider.GetComponent<ShipController>().hasPowerUp == false && collider.gameObject.transform.root.GetComponent<PhotonView> ().isMine) 
			{
				player = collider.gameObject;
				myShip = player.GetComponent<ShipController> ();
				player.GetComponent<ShipController>().hasPowerUp = true;
				PickUp ();
			UIManager.instance.BGM.clip = UIManager.instance.playerSounds [5];
			UIManager.instance.BGM.Play ();
			}
		//}
       // }
    }

    void PickUp()
    {
        pickedUp = true;
		//player.GetComponent<ShipController>().hasPowerUp = true;
        player.GetComponent<PlayerPowerUps>().PickUpNewPowerUp(myPowerUpType);
		UIManager.instance.powerUpIcon.SetActive (true);
		photonView.RPC ("DestroyPowerUp", PhotonTargets.MasterClient);
    }

    #endregion
    #region PostPickUP
    public void init()
    {
        //Initilize values once picked up
        myShip = GetComponent<ShipController>();
        pickedUp = true;
    }

    public void PowerUpFinish()
    {
		
        GetComponent<PlayerPowerUps>().myCurrentPowerUp = null;
		UIManager.instance.powerUpIcon.SetActive (false);

        Destroy(this);
    }

    //Virtual functions

    public virtual void RunPowerup()
    {
        print("Running powerup");
    }
    #endregion


	[PunRPC]
	public void DestroyPowerUp()
	{
		if(photonView.isMine)
		PhotonNetwork.Destroy(this.gameObject);
	}
}
