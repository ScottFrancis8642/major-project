﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUpManager : Photon.MonoBehaviour
{
    public Transform[] powerUpLocations;
	public Transform[] healthPackLocations;
	public GameObject powerUpScript;
	public string[] powerUps;
  	bool spawningPUp;
	bool spawningHPack;
	bool hasStarted;



	//public SpawnCheckerPowerUp checkScript;

	//public List<int> powerUpIndexs = new List<int>();
	//private List<int> powerUpIndexCache = new List<int>();

    void OnJoinedRoom()
    {
        if (PhotonNetwork.masterClient == PhotonNetwork.player)
        {
			StartCoroutine ("SpawnHealthPacks", 0);
			StartCoroutine ("SpawnPowerUps", 0);
			print ("I spawned the power up");
			hasStarted = true;
        }
    }

	void Awake ()
	{
		//powerUpIndexCache = powerUpIndexs;
	
	}

	void Update()
	{

		if (PhotonNetwork.masterClient == PhotonNetwork.player && hasStarted)
		{
			if (powerUpScript == null) 
			{
                
				//print ("Pup finished running another");
                if (spawningPUp != true)
                {
					int randomSpawn = Random.Range (0, 30);
                    StartCoroutine("SpawnPowerUps", randomSpawn);
				
                }

			}
			if (spawningHPack != true) 
			{
				int randomSpawn = Random.Range (0, 20);
				StartCoroutine ("SpawnHealthPacks", randomSpawn);
			}
		}
	}

	IEnumerator SpawnPowerUps(float respawnTime)
	{
		spawningPUp = true;

		yield return new WaitForSeconds(respawnTime);


		foreach (Transform t in powerUpLocations) 
		{
			
			if(Physics.CheckSphere (t.position, 0.5f, 1 << 0) == false)
			{
				Debug.Log ("No PowerUP");	
				int index2 = Random.Range (0, powerUps.Length);
				PhotonNetwork.Instantiate (powerUps [index2], t.position, t.rotation, 0);
			}
				
		}

		spawningPUp = false;
	}

	IEnumerator SpawnHealthPacks(float respawnTime)
	{
		spawningHPack = true;

		yield return new WaitForSeconds (respawnTime);

		foreach (Transform h in healthPackLocations) 
		{
			if (Physics.CheckSphere (h.position, 0.25f, 1 << 0) == false) 
			{
				Debug.Log ("No HealthPack");
				PhotonNetwork.Instantiate ("HealthPack", h.position, h.rotation, 0);
			}
		}
		spawningHPack = false;
	}

	/*void OnDrawGizmos ()
	{
		foreach (Transform t in powerUpLocations)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere (t.position, 1f);

		}
	}*/











    
}
