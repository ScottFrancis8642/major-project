﻿using UnityEngine;
using System.Collections;

public class RapidPunch : PowerUp 
{

	public float punchCoolDown;
	public bool rapidPunchRunning;

	void Awake()
	{
        powerUpSprite = Resources.Load<Sprite>("PowerUpImages/Rapid Punch");
    }

    public void Start()
	{
		//Assign my type
		myPowerUpType = powerupType.rapidPunch;
		pTimer = 5;
		punchCoolDown = 0.2f;
		rapidPunchRunning = false;

	}

	public override void RunPowerup()
	{
		base.RunPowerup();

		photonView.RPC("TurnOnObject", PhotonTargets.All, "RapidPunchPUP", true);

		print("doing punch stuff now");

		StartCoroutine(powRunning());

		hasEnded = false;
	}

	public IEnumerator powRunning()
	{
		print("started powerup");
		rapidPunchRunning = true;
		myShip.rangedTimer = punchCoolDown;
	//	myShip.attackTimer = 0;
		yield return new WaitForSeconds(pTimer);
		myShip.rangedTimer = 1.5f;
		//myShip.attackTimer = 0.3f;
		photonView.RPC("TurnOnObject", PhotonTargets.All, "RapidPunchPUP", false);
		print("ended powerUp");
		rapidPunchRunning = false;
		hasEnded = true;

	}


}
