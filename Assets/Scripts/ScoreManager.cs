﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System;

public class ScoreManager : Photon.MonoBehaviour
{
    public static ScoreManager instance;

    public int playerID;

    public string[] players;
	public int[] playerScores;

	public int maxScore;
	public string currentPlayerWinning;

	public int[] orderScores = new int[4];
    
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
		
    }

	void Update ()
	{
		OpenStats ();
	}


	void OpenStats ()
	{
		for (int i = 0; i < PhotonNetwork.playerList.Length; i++) 
		{
			players [i] = PhotonNetwork.playerList [i].name;	
			playerScores [i] = PhotonNetwork.playerList [i].GetScore ();
			//FInd the highest value in player scores array
			maxScore = Mathf.Max (playerScores.ToArray());

			orderScores [i] = playerScores [i];

			Array.Sort (orderScores);
			Array.Reverse (orderScores);

			//Find the player of the highest value, in player scores and store in current player winning string
			if (PhotonNetwork.playerList [i].GetScore () == maxScore) 
			{
				currentPlayerWinning = PhotonNetwork.playerList [i].name;
			}

		}
	}
}