﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

[RequireComponent(typeof(Rigidbody))]
public class ShipController : Photon.MonoBehaviour {

	private Rigidbody rbody;

	public bool grounded;
	public bool isBig;

	public float smoothing = 1;
	private Quaternion targetRotation;
	public GameObject player;

	Rigidbody m_body;
	float m_deadZone = 0.1f;

	public float m_hoverHeight = 7f;
	public float hoverForceFront = 200;
	public float hoverForceBack = 400;
	public List<Transform> HoverPoints = new List<Transform> ();

	public float forwardsBackwardsAcl;
	public float leftRightAcl;
	public float currentSpeed;
	public float topSpeedBig = 6.18f;
	public float pitch = 0;
	float m_currThrustX = 0.0f;
	float m_currThrustY = 0.0f;

	public float m_turnStrength = 5f;
	float m_currTurn = 0.0f;

	public float jumpHeight = 1500;
	public float jumpMulti = 1;
	private float sprintMulti;
	public float speedBoostMulti = 1;
	public int myDamage;
	public int myRangedDamage = 30;
	public int damageMulti = 1;
	public float hitForce;
	public float hitForceUp;
	public float gloveThrusterForceBig = 1500;
	public float gloveThrusterForceSmall = 1000;

	private Animator anim;
	public GameObject punchObject;

	public GameObject punchObjectPosition;
	public float attackTimer = 0.5f;
	public bool rangedAttackUsed = false;
	public float rangedTimer = 1.5f;
	public bool punching = false;
	public float chargeCounter = 0;
	public bool charging = false;
	int m_layerMask;

	public Camera myCamera;
	public bool hasPowerUp;

	public GameObject radarImage;
	public GameObject rapidPunch;
	public GameObject playerRadarPosition;

	public Sprite bigShip;
	public Sprite smallShip;


	public GameObject spectator;

	//public Transform groundCheck;

	public Transform right;

	void Start () 
	{
		
		Cursor.lockState = CursorLockMode.Locked;
		rbody = player.GetComponent<Rigidbody> ();
		isBig = true;
		sprintMulti = 1;
		anim = GetComponent<Animator> ();
		m_layerMask = 1 << LayerMask.NameToLayer("Characters");
		m_layerMask = ~m_layerMask;
		punchObject.GetComponent<BoxCollider> ().enabled = false;
		Physics.IgnoreCollision(punchObject.GetComponent<BoxCollider>(), GetComponent<SphereCollider> ());
	}

	void Update () 
	{
		
		

		currentSpeed = GetComponent<Rigidbody> ().velocity.magnitude * 1.5f;
		pitch = currentSpeed / topSpeedBig;
		if (pitch <= 0.25f) 
		{
			pitch = 0.25f;
		}

		GetComponent<AudioSource> ().pitch = pitch;

		if (rangedTimer < 1) 
		{
				Vector3 originalScale = punchObject.transform.localScale;
				Vector3 destinationScale = new Vector3 (1f, 1f, 1f);
				punchObject.transform.localScale = Vector3.Lerp (originalScale, destinationScale, 0.05f);
		}


		//Sprint ();
		ChangeScale ();
		Jump();
		UsePowerUp ();
		AttackTimer ();
		ChargedMeleAttack ();
		RangedMeleAttack ();
		rangedAttackTimer ();

	
		if (isBig == true) 
		{
			Metrics.instance.timeBig += Time.deltaTime;
		}

		if (isBig == false) 
		{
			Metrics.instance.timeSmall += Time.deltaTime;
		}

		if (Input.GetKeyDown (KeyCode.F)) 
		{
			Metrics.instance.changedScale++;
		}

		if (Input.GetKeyDown ("escape")) 
		{
			Cursor.lockState = CursorLockMode.None;
		}

		if (punching == true) 
		{
			punchObject.GetComponent<BoxCollider> ().enabled = true;

		} 
		else 
		{
			punchObject.GetComponent<BoxCollider> ().enabled = false;

		}

		myDamage = myDamage * damageMulti;

		if (grounded == false) 
		{
			if (isBig == true) 
			{
				rbody.drag = 1.50f;
				forwardsBackwardsAcl = 18;
				leftRightAcl = 18;
			}

			if (isBig == false) 
			{
				rbody.drag = 1.50f;
				forwardsBackwardsAcl = 28;
				leftRightAcl = 28;
			}
		} 
		else 
		{
			if (isBig == true) 
			{
				rbody.drag = 2;
				forwardsBackwardsAcl = 25;
				leftRightAcl = 25;
			}

			if (isBig == false) 
			{
				rbody.drag = 2;
				forwardsBackwardsAcl = 40;
				leftRightAcl = 40;
			}
		}


		RaycastHit hit;

		if (Physics.Raycast (HoverPoints [0].position, HoverPoints [0].TransformDirection (Vector3.down), out hit, 2f)) 
		{
			if(hit.collider.tag == "Ground")
			{
				Debug.DrawLine (HoverPoints [0].position, hit.point, Color.green);
				grounded = true;
			}

		}
		else
		{
			grounded = false;
		}

		if (Physics.Raycast (HoverPoints [2].position, HoverPoints [2].TransformDirection (Vector3.down), out hit, 2f)) 
		{
			if(hit.collider.tag == "Ground")
			{
				Debug.DrawLine (HoverPoints [2].position, hit.point, Color.green);
				grounded = true;
			}

		}
		else
		{
			grounded = false;
		}

		// Main Thrust
	/*	m_currThrustY = 0.0f;
		float aclYAxis = Input.GetAxis("Vertical");
		if (aclYAxis > m_deadZone)
			m_currThrustY = aclYAxis * forwardsBackwardsAcl * sprintMulti * speedBoostMulti;
		else if (aclYAxis < -m_deadZone)
			m_currThrustY = aclYAxis * forwardsBackwardsAcl * sprintMulti * speedBoostMulti;
*/

		if (UIManager.instance.SpectatorMode == true) 
		{
			spectator.SetActive (true);
			spectator.transform.SetParent (null);
			this.gameObject.SetActive (false);
			/*
			GetComponent<FlyCamera> ().enabled = true;
			gameObject.GetComponent<Rigidbody> ().useGravity = false;
			gameObject.GetComponent<NetworkPlayer> ().secondCam.enabled = false;
			gameObject.GetComponent<NetworkPlayer> ().radarCamera.enabled = false;
			gameObject.GetComponentInChildren<Crosshair> ().enabled = false;
	*/
		} 
		else 
		{
			//GetComponent<FlyCamera> ().enabled = false;
			if (Input.GetKey (KeyCode.W)) 
			{
				rbody.AddForce (ContrainedForward (transform) * forwardsBackwardsAcl * sprintMulti * speedBoostMulti);		
			}

			if (Input.GetKey (KeyCode.S)) {
				rbody.AddForce (-ContrainedForward (transform) * forwardsBackwardsAcl * sprintMulti * speedBoostMulti);		
			}

			if (Input.GetKey (KeyCode.A)) {
				rbody.AddForce (Vector3.Normalize (transform.root.position - right.position) * forwardsBackwardsAcl * sprintMulti * speedBoostMulti);	

			}

			if (Input.GetKey (KeyCode.D)) {
				rbody.AddForce (Vector3.Normalize (right.position - transform.root.position) * forwardsBackwardsAcl * sprintMulti * speedBoostMulti);		

			}

		}


		/*m_currThrustX = 0.0f;
		float aclXAxis = Input.GetAxis ("Horizontal");
		if (aclXAxis > m_deadZone)
			m_currThrustX = aclXAxis * leftRightAcl * sprintMulti * speedBoostMulti;
		else if (aclXAxis < -m_deadZone)
			m_currThrustX = aclXAxis * leftRightAcl * sprintMulti * speedBoostMulti;*/

	}

	Vector3 ContrainedForward (Transform fwd)
	{
		Vector3 naturalForward = fwd.forward;
		naturalForward.y = 0;
		Vector3 fixedForward = naturalForward.normalized;
		return fixedForward;
	
	}

	void FixedUpdate()
	{
		for (int i = 0; i < 4; i++) 
		{
			RaycastHit Hit;

			if (i > 1) 
			{
				if (Physics.Raycast (HoverPoints [i].position, HoverPoints [i].TransformDirection (Vector3.down), out Hit, m_hoverHeight)) 
				{
					rbody.AddForceAtPosition ((Vector3.up * hoverForceBack * Time.deltaTime) * Mathf.Abs (1 - (Vector3.Distance (Hit.point, HoverPoints [i].position) / m_hoverHeight)), HoverPoints [i].position);
					//grounded = true;

					if (Hit.point != Vector3.zero) 
					{
						
						Debug.DrawLine (HoverPoints [i].position, Hit.point, Color.blue);

					} 


				} 


			}		
			else 
			{	
				if (Physics.Raycast (HoverPoints [i].position, HoverPoints [i].TransformDirection (Vector3.down), out Hit, m_hoverHeight)) 
				{
					rbody.AddForceAtPosition ((Vector3.up * hoverForceFront * Time.deltaTime) * Mathf.Abs (1 - (Vector3.Distance (Hit.point, HoverPoints [i].position) / m_hoverHeight)), HoverPoints [i].position);
					//grounded = true;
					if (Hit.point != Vector3.zero) 
					{
							
						Debug.DrawLine (HoverPoints [i].position, Hit.point, Color.red);
					} 
					else 
					{
							
					}
				} 
				else 
				{
					//grounded = false;
				}
			}
		}
	

		// Forward
		if (Mathf.Abs (m_currThrustY) > 0) 
		{
			rbody.AddForce (transform.forward * m_currThrustY);
		}
		// Turn
		if (Mathf.Abs(m_currThrustX) > 0) 
		{

			rbody.AddForce(transform.right * m_currThrustX);
		} 

	}

	#region Jumping
	void Jump()
	{
		if (Input.GetButtonDown ("Jump") && grounded == true) 
		{
			
			rbody.AddForce (Vector3.up * jumpHeight * jumpMulti);
			anim.SetTrigger ("Jump");
			UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [7]);

		}
	}
	#endregion

	#region Ranged Punch

	void RangedMeleAttack ()
	{
		if (Customization.instance.playerColors[PhotonNetwork.player.ID-1].Equals(0)) 
		{
			if (Input.GetMouseButtonDown (1) && punching == false && rangedAttackUsed == false && charging == false && !UIManager.instance.SpectatorMode) 
			{
				if (isBig == true) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveRed", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceBig);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}

				}

				if (isBig == false) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveSmallRed", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceSmall);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}
				}

			}
		}
		if (Customization.instance.playerColors[PhotonNetwork.player.ID-1].Equals(1)) 
		{
			if (Input.GetMouseButtonDown (1) && punching == false && rangedAttackUsed == false && charging == false && !UIManager.instance.SpectatorMode) 
			{
				if (isBig == true) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveBlue", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceBig);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}

				}

				if (isBig == false) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveSmallBlue", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceSmall);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}
				}

			}
		}
		if (Customization.instance.playerColors[PhotonNetwork.player.ID-1].Equals(2)) 
		{
			if (Input.GetMouseButtonDown (1) && punching == false && rangedAttackUsed == false && charging == false && !UIManager.instance.SpectatorMode) 
			{
				if (isBig == true) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveBlack", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceBig);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}

				}

				if (isBig == false) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveSmallBlack", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceSmall);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}
				}

			}
		}
		if (Customization.instance.playerColors[PhotonNetwork.player.ID-1].Equals(3)) 
		{
			if (Input.GetMouseButtonDown (1) && punching == false && rangedAttackUsed == false && charging == false && !UIManager.instance.SpectatorMode) 
			{
				if (isBig == true) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveYellow", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceBig);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}

				}

				if (isBig == false) 
				{
					if (photonView.isMine) 
					{
						punchObject.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
						GameObject Glove = (GameObject)PhotonNetwork.Instantiate ("RangedGloveSmallYellow", punchObjectPosition.transform.position, punchObjectPosition.transform.rotation, 0);
						Glove.GetComponent<GloveRanged> ().myship = this.gameObject;
						anim.SetTrigger ("RangedPunch");
						Glove.GetComponent<Rigidbody> ().AddForce (Glove.transform.right * gloveThrusterForceSmall);
						Physics.IgnoreCollision (Glove.GetComponent<BoxCollider> (), player.GetComponent<SphereCollider> ());
						rangedAttackUsed = true;
						punching = true;
						UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [2]);
					}
				}

			}
		}
	}

	#endregion

	#region charged melee attack
	void ChargedMeleAttack ()
	{
		if (Input.GetMouseButtonDown (0) && rangedAttackUsed == false)
			{
			UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [4]);
			}

		if (Input.GetMouseButton (0) && punching == false && rangedAttackUsed == false) 
		{
			if (isBig == true) 
			{
				chargeCounter += Time.deltaTime;
				myDamage = Mathf.RoundToInt (chargeCounter * 34);
				hitForce += Time.deltaTime * 2750;
				hitForceUp += Time.deltaTime * 1250;

				Vector3 originalScale = punchObject.transform.localScale;
				Vector3 destinationScale = new Vector3 (2f, 2f, 2f);
				punchObject.transform.localScale = Vector3.Lerp (originalScale, destinationScale, 0.025f);

				charging = true;

				if (hitForce >= 5000) 
				{
					hitForce = 5000;
				}

				if(hitForceUp >= 2000)
				{
					hitForceUp = 2000;
				}
			}

			if (isBig == false) 
			{
				chargeCounter += Time.deltaTime;
				myDamage = Mathf.RoundToInt (chargeCounter * 15);
				hitForce += Time.deltaTime * 700;
				hitForceUp += Time.deltaTime * 150;
				charging = true;

				Vector3 originalScale = punchObject.transform.localScale;
				Vector3 destinationScale = new Vector3 (2f, 2f, 2f);
				punchObject.transform.localScale = Vector3.Lerp (originalScale, destinationScale, 0.025f);

				if (hitForce >= 1000) 
				{
					hitForce = 1000;
				}

				if (hitForceUp >= 250) 
				{
					hitForceUp = 250;
				}
			}

			if (chargeCounter >= 1.5f) 
			{
				chargeCounter = 1.5f;
			} 

		}


		if (Input.GetMouseButtonUp (0) && punching == false) 
		{
			punching = true;
			charging = false;
			photonView.RPC ("PlayPunchAnimation", PhotonTargets.All);

			chargeCounter = 0;

			UIManager.instance.BGM.Stop ();
			UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [0]);

			StartCoroutine (ResetDamage (0.4f));


		}
	}
		
	#endregion

	#region Using powerUp
	void UsePowerUp ()
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			if (GetComponent<PlayerPowerUps>().myStoredPowerUp != null)
			{
				Metrics.instance.powerupsUsed++;
				UIManager.instance.powerUpIcon.GetComponent<Animator> ().SetTrigger ("UsingPowerUp");
				GetComponent<PlayerPowerUps>().RunPowerUp();
			}
		}
	}
	#endregion

	#region sprinting
	void Sprint ()
	{
		if (Input.GetButton ("Fire3")  )
		{
			sprintMulti = 1.5f;
		} 
		else 
		{
			sprintMulti = 1;
		}
	}
	#endregion

	#region Scale
	void ChangeScale ()
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) && isBig == true)
		{
			myCamera.fieldOfView = 90;
			myCamera.GetComponent<Fisheye> ().strengthX = 0.2f;
			myCamera.GetComponent<Fisheye> ().strengthY = 0.2f;
			player.transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f);
			forwardsBackwardsAcl = 40;
			leftRightAcl = 30;
			m_hoverHeight = 0.4f;
			jumpHeight = 1000;
			myDamage = 0;
			myRangedDamage = 15;
			hitForce = 100;
			hitForceUp = 25;
			isBig = false;
			radarImage.transform.localScale = new Vector3 (60, 60, 60);
			playerRadarPosition.transform.localScale = new Vector3 (10, 10, 10);
			UIManager.instance.sizeImage.sprite = smallShip;
			UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [6]);
			UIManager.instance.shrinkBlur.enabled = true;

		}
		else if (Input.GetKeyDown(KeyCode.LeftShift) && isBig == false)
		{
			myCamera.fieldOfView = 80;
			myCamera.GetComponent<Fisheye> ().strengthX = 0f;
			myCamera.GetComponent<Fisheye> ().strengthY = 0f;
			player.transform.localScale = new Vector3 (1f, 1f, 1f);
			forwardsBackwardsAcl = 25;
			leftRightAcl = 25;
			m_hoverHeight = 0.8f;
			jumpHeight = 1500;
			myDamage = 0;
			myRangedDamage = 30;
			hitForce = 500;
			hitForceUp = 100;
			isBig = true;
			radarImage.transform.localScale = new Vector3 (12, 12, 12);
			playerRadarPosition.transform.localScale = new Vector3 (2, 2, 2);
			UIManager.instance.sizeImage.sprite = bigShip;
			UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [6]);
			UIManager.instance.shrinkBlur.enabled = false;
		}
	}
	#endregion

	#region AttackTimer

	void AttackTimer ()
	{
		if (punching == true) 
		{
			attackTimer -= Time.deltaTime;

			if (attackTimer <= 0 && rangedAttackUsed == false) 
			{
				punchObject.transform.localScale = new Vector3 (1f, 1f, 1f);
			
				attackTimer = 0.5f;
				punching = false;

			}
		}
	}
	#endregion

	void rangedAttackTimer ()
	{
		if (rangedAttackUsed == true) 
		{
			rangedTimer -= Time.deltaTime;

			if (rangedTimer <= 0) 
			{
				rangedAttackUsed = false;
				punching = false;
				if (GetComponent<RapidPunch>() == null)
				{
					rangedTimer = 1f;
				}
				else if (GetComponent<RapidPunch> ().rapidPunchRunning == true) 
				{
					rangedTimer = GetComponent<RapidPunch> ().punchCoolDown;
				}

			}
		}
	}

	IEnumerator ResetDamage(float waitTime)
	{
		yield return new WaitForSeconds (waitTime);

		if (isBig == true) 
		{
			
			myDamage = 0;
			hitForce = 500;
			hitForceUp = 100;
		}

		if (isBig == false) 
		{
			
			myDamage = 0;
			hitForce = 100;
			hitForceUp = 25;
		}
	}




}