﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class ShowScore : Photon.MonoBehaviour 
{
	//public Text deaths;
	//public Text timeAsKing;
	public GameObject[] scoreBoardPanels;
	NetworkManager netman;

	void Start()
	{
		photonView.RPC ("OpenStats", PhotonTargets.AllBufferedViaServer);
	}

	[PunRPC]
	void OpenStats ()
	{
		foreach (PhotonPlayer p in PhotonNetwork.playerList) 
		{
			for (int i = 0; i < 4; i++) 
			{
				if ((PhotonNetwork.player.ID - 1) == i) 
				{
					scoreBoardPanels [i].GetComponent<Image> ().color = Color.green;
					string[] s = File.ReadAllLines ("C:\\Users\\Public\\DocumentsAnalysis.txt");
					scoreBoardPanels[i].GetComponentInChildren<Text>().text = s[0] + '\n' + s[1] + '\n' + s [2] + '\n' + s [3] + '\n' + s [4] + '\n' + s [8]  + '\n' + s [9] + '\n' + s [10] + '\n' + s [11] + '\n' + s[12] + '\n' + '\n' + s[13];
				} 
				else 
				{
					scoreBoardPanels  [i].GetComponent<Image> ().color = Color.white;
				}
			}
		}
	}
}

//writer.WriteLine (PhotonNetwork.player.name + " stats:  '\n'");
//writer.WriteLine ("Glove Kills: " + (meleePunchesHit + rangedPunchesHit)); 		//3
//writer.WriteLine ("Time big: " + timeBig);    									//4
//writer.WriteLine ("Time small: " + timeSmall);									//5
//writer.WriteLine ("Changed scale: " + changedScale);								//6
//writer.WriteLine ("Deaths as big: " + deathsAsBig);								//7
//writer.WriteLine ("Deaths as small: " + deathsAsSmall);							//8
//writer.WriteLine ("Suicides: " + suicides);										//9
//writer.WriteLine ("Environment kills: " + environmentKills);						//10
//writer.WriteLine ("Environment deaths: " + environmentDeaths);					//11
//writer.WriteLine ("Powerups Used: " + powerupsUsed);								//12