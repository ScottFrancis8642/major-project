﻿using UnityEngine;
using System.Collections;

public class SpawnChecker : MonoBehaviour {

	public bool playerInSpawn;
	public GameObject mySpawn;

	void OnTriggerStay ()
	{
		playerInSpawn = true;
	}

	void OnTriggerExit ()
	{
		playerInSpawn = false;
	}
}
