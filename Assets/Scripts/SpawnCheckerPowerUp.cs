﻿using UnityEngine;
using System.Collections;

public class SpawnCheckerPowerUp : MonoBehaviour {

	public bool powerupInSpawn = false;

	void OnTriggerEnter (Collider col)
	{
		if(col.gameObject.tag == "Powerup")
		{
			powerupInSpawn = true;
		}
	}

	void OnTriggerStay (Collider col)
	{
		if(col.gameObject.tag == "Powerup")
		{
			powerupInSpawn = true;
		}
	}

	void OnTriggerExit (Collider col)
	{
		if(col.gameObject.tag == "Powerup")
		{
			powerupInSpawn = false;
		}
	}
}
