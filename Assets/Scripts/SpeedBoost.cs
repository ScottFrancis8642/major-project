﻿using UnityEngine;
using System.Collections;

public class SpeedBoost : PowerUp 
{

    float boostAmount;


	void Awake()
	{
		powerUpSprite = Resources.Load<Sprite>("PowerUpImages/SpeedIncreaseIcon");

	}

    public void Start()
    {
        //Assign my type
        myPowerUpType = powerupType.speedBoost;
        pTimer = 8;
        boostAmount = 2f;

    }

	public override void RunPowerup()
    {
        base.RunPowerup();

		photonView.RPC("TurnOnObject", PhotonTargets.All, "SpeedBoostPUP", true);

        print("doing speed stuff now");

        StartCoroutine(powRunning());

		hasEnded = false;
    }

    public IEnumerator powRunning()
    {
        print("started powerup");
		myShip.speedBoostMulti = boostAmount;
        yield return new WaitForSeconds(pTimer);
		myShip.speedBoostMulti = 1;
		photonView.RPC("TurnOnObject", PhotonTargets.All, "SpeedBoostPUP", false);
        print("ended powerUp");
        hasEnded = true;

    }



  
}
