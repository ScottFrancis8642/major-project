﻿using UnityEngine;
using System.Collections;

public class SplashDamage : Photon.MonoBehaviour
{

	public float waitTime = 0.1f;
	public GameObject myship;


	// Use this for initialization
	void Start () 
	{


		StartCoroutine (DestroyParticle (waitTime));
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter (Collider col)
	{
		

		if (col.gameObject.tag == "Player") 
		{
			myship.GetComponentInChildren<Crosshair> ().showGUI = true;
			UIManager.instance.BGM.PlayOneShot (UIManager.instance.playerSounds [3]);
			PhotonView pv = col.transform.root.GetComponent<PhotonView> ();
			pv.RPC ("ReduceHealth", PhotonTargets.All, myship.GetComponent<ShipController>().myRangedDamage,  GetComponent<PhotonView>().ownerId, myship.GetPhotonView().viewID, myship.transform.position, myship.GetComponent<ShipController>().hitForce, myship.GetComponent<ShipController>().hitForceUp);
			PhotonNetwork.Destroy (this.gameObject);
		}

	}

	IEnumerator DestroyParticle (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		if(photonView.isMine)
		PhotonNetwork.Destroy (this.gameObject);
	}


}
