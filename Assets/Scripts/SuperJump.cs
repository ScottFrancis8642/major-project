﻿using UnityEngine;
using System.Collections;

public class SuperJump : PowerUp 
{

	float JumpAmount;


	void Awake()
	{
		powerUpSprite = Resources.Load<Sprite>("PowerUpImages/SuperJumpIcon");
	}

	public void Start()
	{
		//Assign my type
		myPowerUpType = powerupType.superJump;
		pTimer = 8;
		JumpAmount = 2.5f;
	

	}

	public override void RunPowerup()
	{
		base.RunPowerup();

		photonView.RPC("TurnOnObject", PhotonTargets.All, "SuperJumpPUP", true);

		print("doing jump stuff now");

		StartCoroutine(powRunning());

		hasEnded = false;
	}

	public IEnumerator powRunning()
	{
		print("started powerup");

		myShip.jumpMulti = JumpAmount;
		yield return new WaitForSeconds(pTimer);
		myShip.jumpMulti = 1;
		photonView.RPC("TurnOnObject", PhotonTargets.All, "SuperJumpPUP", false);
		print("ended powerUp");

		hasEnded = true;
	}


}

