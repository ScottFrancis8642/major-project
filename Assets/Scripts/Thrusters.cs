﻿using UnityEngine;
using System.Collections;

public class Thrusters : MonoBehaviour {

	public float thrusterStrength;
	public float thrusterDistance;
	public Transform[] thrusters;

	void FixedUpdate () 
	{
		RaycastHit hit;
		foreach (Transform thruster in thrusters) 
		{
			Vector3 downwardForce;
			float distancePercentage;

			if (Physics.Raycast (thruster.position, thruster.up * -1, out hit, thrusterDistance)) 
			{
				distancePercentage = 1 - (hit.distance / thrusterDistance);
				downwardForce = transform.up * thrusterStrength * distancePercentage;
				downwardForce = downwardForce * Time.deltaTime * gameObject.GetComponent<Rigidbody>().mass;
				gameObject.GetComponent<Rigidbody> ().AddForceAtPosition (downwardForce, thruster.position);
			}
		}
	}
}
