﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class UIManager : MonoBehaviour 
{
	// Declare the items we want for HUD
	public static UIManager instance;
	public GameObject[] panels;
	public GameObject standbyCamera;
	public GameObject levelViewStandbyCamera;

	public Text[] playerNames;

    [Header("Display Button and Character Screen")]
	public GameObject JoinGameBTN;
    public GameObject HostGameBTN;
	public GameObject CharacterSelectScreen;

    [Header("Text options")]
    public Text KillsText;
	public Text healthText;
	public Text controlPoints;
	public Text kingPoints;
    public Text PlayersInCp;
    public Text PlayersInCpNumber;

	[Header("Timer Attributes")]
	public Text timerText;
	public float countDownTimer = 300f;
	public float currentTime;
	public string countDownMinutes;
	public string countDownSeconds;

    #region Stats

    [Header("UI Stats")]
    public AudioSource BGM;
	public AudioClip[] playerSounds;
	public GameObject powerUpIcon;

	public Image playerHealthBar;
	public Image controlPointsRadial;
	public Image sizeImage;
	public Image KingCrownImage;

	public Image warningGlow;
	public Image cracks;
	public Image shrinkBlur;
	public bool SpectatorMode;
	public GameObject ctrlPnt;

	#endregion Stats

	void Awake()
	{
		currentTime = countDownTimer;
		instance = this;
	}

	void Start()
	{
    }

	
    void Update()
	{
		KillsText.text =  PhotonNetwork.player.GetScore ().ToString();
		PlayersInCpNumber.text = ctrlPnt.GetComponent<ControlPoint> ().playersInRange.Count.ToString ();

		foreach (PhotonPlayer p in PhotonNetwork.playerList) 
		{
			if (p.ID == 1) 
			{
				playerNames [0].text = p.name;
			
			} 
			else if (p.ID == 2) 
			{
				playerNames [1].text = p.name;

			} 
			else if (p.ID == 3) 
			{
				playerNames [2].text = p.name;

			}
			else if (p.ID == 4)
			{
				playerNames [3].text = p.name;

			}
		}

		if (PhotonNetwork.inRoom) 
		{
			for (int i = 0; i < 4; i++) {
				if ((PhotonNetwork.player.ID - 1) == i) {
					panels [i].GetComponent<Image> ().color = Color.green;

				} else {
					
					Button[] buttons = panels [i].GetComponentsInChildren<Button> ();


					foreach (Button b in buttons) {
						b.gameObject.SetActive (false);
					}

					panels [i].GetComponent<Image> ().color = Color.white;
				
				}
			}

		}
		for (int i = 0; i < 4; i++) {
		
			if (i >= PhotonNetwork.playerList.Length)
				playerNames [i].text = "Empty";

		}

//		for (int i = 0; i < PhotonNetwork.playerList.Length; i++) 
//		{
//			playerNames [i].text = PhotonNetwork.playerList [i].name;
//		}
	}

	public void Spectate()
	{
		SpectatorMode = true;
	}
   
}