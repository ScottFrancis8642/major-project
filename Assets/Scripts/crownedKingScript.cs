﻿using UnityEngine;
using System.Collections;

public class crownedKingScript : Photon.MonoBehaviour
{
    public bool cannotTake;
	public GameObject playerWhoIsKing;
	public bool freeCrown;


    void OnTriggerEnter(Collider col)
    {
		if (col.gameObject.tag == "Player" && freeCrown == true) 
		{
			col.GetComponent<PhotonView>().RPC("TurnOnIsKing", PhotonTargets.All); 
			freeCrown = false;
		}
    }

	void Update ()
	{
		if (playerWhoIsKing != null) 
		{
			transform.position = new Vector3 (playerWhoIsKing.transform.position.x, playerWhoIsKing.transform.position.y + 1, playerWhoIsKing.transform.position.z);
			transform.rotation = playerWhoIsKing.transform.rotation;

		}


		if (freeCrown == false) 
		{
			GetComponent<SphereCollider> ().enabled = false;
		} 
		else if (freeCrown == true) 
		{
			GetComponent<SphereCollider> ().enabled = true;
		}

		if(freeCrown == true)
			transform.position = transform.parent.transform.position;
	


	}

	[PunRPC]
	void FreeCrown ()
	{
		freeCrown = true;

	}
		
}

