﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class propShipScript : Photon.MonoBehaviour
{
    [Header("Display Ship Holders")]
	public int shipID;
	public GameObject[] propPlayerShipModels;
	public GameObject[] propPlayer2ShipModels;
	public GameObject[] propPlayer3ShipModels;
    public GameObject[] propPlayer4ShipModels;

	private GameObject submitButton;

	public Sprite[] playerColorSprites; //These are our reference images.

	public GameObject[] UIShipImages; //This si the one you see

	void Update()
	{
		UpdateDisplay ();
	}

	public void UpdateDisplay()
    {
		for (int i = 0; i < 4; i++) 
		{
			UIShipImages [i].GetComponent<Image> ().sprite = playerColorSprites [Customization.instance.playerColors [i]];
		}

	}
}